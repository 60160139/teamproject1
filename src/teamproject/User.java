/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teamproject;

/**
 *
 * @author informatics
 */
public class User {
    private int ID ;
    private String username  ;
    private String password  ;
    private String name  ;
    private String surname ;
    private String tel ;
    private int age ;
    private double weight ;
    private double height ;

    public User() {
    }
    public User(int ID, String username, String password, String name, String surname, String tel, int age, double weight,double height) {
        this.ID = ID;
        this.username = username;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.tel = tel;
        this.age = age;
        this.weight = weight;
        this.height = height;
    }

    public int getID() {
        return ID;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getTel() {
        return tel;
    }

    public int getAge() {
        return age;
    }

    public double getWeight() {
        return weight;
    }

    public double getHeight() {
        return height;
    }
    
}
